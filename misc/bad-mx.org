** Understanding Required
   :PROPERTIES:
   :CUSTOM_ID: bad-mx
   :reveal_extra_attr: data-audio-src="./audio/6-bad-mx.ogg"
   :END:
   - Sample scenario (inspired by student’s exam attempt)
     - All threads share some object ~shared~
     - Every thread has its own ~number~
   - The following does *not* work!
     ([[https://gitlab.com/oer/OS/blob/master/java/SetGetFail.java][Full source code]])
     - This serves as *bad* example, don’t do this
       #+BEGIN_SRC java
/* Following method called from run().
   Repeatedly assign this thread's own number to a
   shared object and race to read that number again.
   Exit, if other thread modified number in between. */
public synchronized void setGet() {
  while(true) {
    shared.setNo(number);
    int no = shared.getNo();
    if (no != number) {
      System.out.println("Thread " + number + " sees " + no);
      System.exit(no); }}}
       #+END_SRC
#+BEGIN_NOTES
The previous slide contains an example for MX with a synchronized
method sell().
This slide seems to contain a similar example for MX with a synchronized
method setGet().
However, this slide serves as counter example, and you should figure
out why it does not work in the next JiTT assignment.
#+END_NOTES

** Exercise task
   :PROPERTIES:
   :CUSTOM_ID: exercise-6
   :reveal_data_state: no-toc-progress
   :reveal_extra_attr: class="jitt"
   :END:

   Submit your solution in {{{jittquiz}}}.

   Inspect and understand, compile, and run the [[https://gitlab.com/oer/OS/blob/master/java/SetGetFail.java][Java program containing the code from the previous slide]].
   The intention of introducing ~synchronized~ was to create a
   critical section containing ~shared.setNo(number)~ and
   ~shared.getNo()~, with the effect that ~no~ always equals ~number~.

   What output do you observe when running the program?  What does
   that tell you about critical sections or races involving ~shared~?
   What is locked thanks to ~synchronized~ in that code?
